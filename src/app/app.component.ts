import { Component } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { LoginService } from './_service/login.service';
import { MenuService } from './_service/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  styles: [
    "node_modules/primeicons/primeicons.css",
    "node_modules/primeng/resources/themes/nova-light/theme.css",
    "node_modules/primeng/resources/primeng.min.css"
    //...
  ],
  providers: [MessageService]
})
  export class AppComponent {
    title = 'primeng-crud';
    menuItems: MenuItem[] = [];

    constructor(private menuService: MenuService, private messageService: MessageService, public loginService: LoginService) {}

    ngOnInit() {
      this.menuService.menuCambio.subscribe(data => {
        this.menuItems = [];
        console.log(data);
        data.forEach(value => {
          if (value.idPadre === 0 ) {
            let item: MenuItem;
            if ( value.url === '' ) {
              item = {label: value.nombre, icon: value.icono};
            } else {
              item = {label: value.nombre, icon: value.icono, routerLink: [value.url]};
            }

            const menuItemsSub: MenuItem[] = [];
            data.forEach(valueSub => {
              if (value.idMenu === valueSub.idPadre) {
                let itemSub: MenuItem;
                if ( valueSub.url === '' ) {
                  itemSub = {label: valueSub.nombre, icon: valueSub.icono};
                } else {
                  itemSub = {label: valueSub.nombre, icon: valueSub.icono, routerLink: [valueSub.url]};
                }
                menuItemsSub.push( itemSub );
              }
            });
            item.items = menuItemsSub;
            this.menuItems.push( item );
          }
        });

        console.log(this.menuItems);

      });

    }
}
