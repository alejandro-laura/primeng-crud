import { Component, OnInit } from '@angular/core';
import { Alumno } from 'src/app/_model/alumno';
import { AlumnoService } from 'src/app/_service/alumno.service';
import { MessageService, SelectItem } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html',
  styleUrls: ['./alumno.component.css'],
  providers: [MessageService]
})
export class AlumnoComponent implements OnInit {

  alumnos: Alumno[];
  cols: any[];
  cantidad: number;
  selectedAlumno: Alumno;
  seeTable: boolean = true;
  apellidos: SelectItem[];
  fNombres: String;
  fApellidos: String;

  constructor(private alumnoService: AlumnoService
    , private messageService: MessageService , private router: Router) {
      this.alumnoService.seeTable.next(true);
  }

  ngOnInit() {
    
    /*this.alumnoService.listAll().subscribe(data => this.alumnos = data);*/
    this.alumnoService.seeTable.next(true);
    this.alumnoService.listPageable(0, 3).subscribe(data => {
      console.log(data);
      this.alumnos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
    });
    this.cols = [
        { field: 'idAlumno', header: 'id' },
        { field: 'nombres', header: 'Nombres' },
        { field: 'apellidos', header: 'Apellidos' }
    ];
    this.alumnoService.seeTable.subscribe(data => {
      this.seeTable = data;
    });
    this.apellidos = [
      { label: 'All', value: null },
      { label: 'Laura', value: 'Laura' },
      { label: 'Aguilar', value: 'Aguilar' }
    ];
  }
  paginate(event: any) {
    //event.first = Index of the first record
    //event.rows = Number of rows to display in new page
    //event.page = Index of the new page
    //event.pageCount = Total number of pages
    this.alumnoService.listPageable(event.page, event.rows).subscribe(data => {
      console.log(data);
      this.alumnos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
    });
  }

  eliminar(id: number) {
    this.alumnoService.delete(id).subscribe(data => {
      this.alumnoService.listPageable(0, 3).subscribe(data => {
        console.log(data);
        this.alumnos = JSON.parse(JSON.stringify(data)).content;
        this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      });
    }
    );
  }

  selectCarWithButton(alumno: Alumno) {
    this.selectedAlumno = alumno;
    this.messageService.add({severity:'info', summary:'Car Selected', detail:'Vin: ' + alumno.idAlumno});
  }

  onRowSelect(event) {
      this.messageService.add({severity:'info', summary:'Car Selected', detail:'Vin: ' + event.alumno.idAlumno});
  }

  onRowUnselect(event) {
      this.messageService.add({severity:'info', summary:'Car Unselected', detail:'Vin: ' + event.alumno.idAlumno});
  }

  editar(){
    console.log('alejo');
    this.router.navigate(['alumno', 'edicion', this.selectedAlumno.idAlumno]);
  }
  
  filterNombres(event: any, field: String){
    /*if(event.keyCode != 13){
      return;
    }*/
    console.log(event);
    console.log('field ' + field);
  }
  filterApellidos(value: String, field: String){
    console.log(value);
    console.log('field ' + field);
  }
}
