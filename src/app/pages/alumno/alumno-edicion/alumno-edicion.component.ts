import { Component, OnInit } from '@angular/core';
import { Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import { Alumno } from 'src/app/_model/alumno';
import { AlumnoService } from 'src/app/_service/alumno.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-alumno-edicion',
  templateUrl: './alumno-edicion.component.html',
  styleUrls: ['./alumno-edicion.component.css']
})


export class AlumnoEdicionComponent implements OnInit {

  constructor(private fb: FormBuilder,
    private pacienteService: AlumnoService, private route: ActivatedRoute, private router: Router) {
      this.paciente = new Alumno();
  }

  userform: FormGroup;
  submitted: boolean;
  description: string;

  id: number;
  paciente: Alumno;
  edicion: boolean = false;

  ngOnInit() {
    this.pacienteService.seeTable.next(false);
    this.userform = this.fb.group({
      'id': new FormControl(''),
      'nombres': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
      'apellidos': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)]))
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }


  private initForm() {
    if (this.edicion) {
      this.pacienteService.listById(this.id).subscribe(data => {
        this.userform = this.fb.group({
          'id': new FormControl(data.idAlumno),
          'nombres': new FormControl(data.nombres, Validators.compose([Validators.required, Validators.minLength(3)])),
          'apellidos': new FormControl(data.apellidos, Validators.compose([Validators.required, Validators.minLength(3)]))
        });
      });
    }
  }

/*
  onSubmit(value: string) {
    console.log(value);
    this.submitted = true;
  }
*/
  get diagnostic() { 
    return JSON.stringify(this.userform.value);
  }


  operar() {
    this.paciente.idAlumno = this.userform.value['id'];
    this.paciente.nombres = this.userform.value['nombres'];
    this.paciente.apellidos = this.userform.value['apellidos'];

    if (this.edicion) {
      //update
      this.pacienteService.update(this.paciente).subscribe(data => {
        this.pacienteService.listPageable(0, 3).subscribe(pacientes => {
          //this.pacienteService.pacienteCambio.next(pacientes);
          this.pacienteService.mensaje.next('Se modificó');
        });
      });
    } else {
      //insert
      this.pacienteService.save(this.paciente).subscribe(data => {
        this.pacienteService.listPageable(0, 3).subscribe(pacientes => {
          //this.pacienteService.pacienteCambio.next(pacientes);
          this.pacienteService.mensaje.next('Se registró');
        });
      });
    }
    this.pacienteService.seeTable.next(true);
    this.router.navigate(['alumno'])
  }  

  cancelar(){
    this.pacienteService.seeTable.next(true);
    this.router.navigate(['alumno']);
  }
}




