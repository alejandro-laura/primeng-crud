import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlumnoComponent } from './pages/alumno/alumno.component';
import { AlumnoEdicionComponent } from './pages/alumno/alumno-edicion/alumno-edicion.component';
import { LoginComponent } from './login/login.component';
import { GuardService } from './_service/guard.service';
import { PersonaComponent } from './pages/persona/persona.component';
import { Not403Component } from './pages/not403/not403.component';

const routes: Routes = [
  { path: 'not-403', component: Not403Component },
  {path: 'alumno', component: AlumnoComponent, children:[
    {path: 'nuevo', component: AlumnoEdicionComponent},
    {path: 'edicion/:id', component: AlumnoEdicionComponent}
  ], canActivate: [GuardService] },
  {path: 'persona', component: PersonaComponent, canActivate: [GuardService] },
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
