
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { PaginatorModule } from 'primeng/paginator';
import { InputTextModule } from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {ToastModule} from 'primeng/toast';
import {MessageModule} from 'primeng/message';
import {PanelModule} from 'primeng/panel';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import {PasswordModule} from 'primeng/password';
import {ToolbarModule} from 'primeng/toolbar';
import {MenubarModule} from 'primeng/menubar';
import {SplitButtonModule} from 'primeng/splitbutton';

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        ButtonModule,
        TableModule,
        DropdownModule,
        PaginatorModule,
        InputTextModule,
        CardModule,
        ToastModule,
        MessageModule,
        PanelModule,
        InputTextareaModule,
        TabViewModule,
        CodeHighlighterModule,
        PasswordModule,
        ToolbarModule,
        MenubarModule,
        SplitButtonModule
    ],
    exports: [
        ButtonModule,
        TableModule,
        DropdownModule,
        PaginatorModule,
        InputTextModule,
        CardModule,
        ToastModule,
        MessageModule,
        PanelModule,
        InputTextareaModule,
        TabViewModule,
        CodeHighlighterModule,
        PasswordModule,
        ToolbarModule,
        MenubarModule,
        SplitButtonModule
    ],
    providers: [],
    declarations: []
})
export class PrimeModule { }
